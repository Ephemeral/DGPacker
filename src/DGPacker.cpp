#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>

#include "..\\include\\MZFileSystem.h"
#include "..\\include\\Unzipper.h"


#include "..\\include\\Zipper.h"

void StripExtention(char* FilePath);
void ReplaceExtension(char* FilePath,char* Extention);
void StripModuleFromPath(char* FilePath);

void DisplayUsage()
{
	printf("Usage: DGPacker.exe d|c filename\n\t\t\td - decompress archive to directory\n\t\t\tc - compress directory to archive\n");
	printf("Examples:\n");
	printf("\tDGPacker.exe d system.de\n\t\t(unpacked system.de to directory ./system/ )\n");
	printf("\tDGPacker.exe c system\n\t\t(make system.de and pack files ./system/*.* to it)\n");
}
int main(int argc, char** argv)
{
	if(argc < 3)
	{
		DisplayUsage();
	}
	else
	{
		if(!strcmp(argv[1],"c"))
		{
			CHAR CurrentFile[MAX_PATH] = {0};
			MZip Convert;

			
			GetCurrentDirectory(sizeof(CurrentFile),CurrentFile);

			SetCurrentDirectory(argv[2]);
			
			printf("Packing %s\n",argv[2]);
			CZipper::ZipFolder(".",false);

			//SetCurrentDirectory(CurrentFile);
			strcat(CurrentFile,"\\");
			strcat(CurrentFile,argv[2]);
			strcat(CurrentFile,".de");
			MoveFileExA(".zip",CurrentFile,MOVEFILE_REPLACE_EXISTING);
			DeleteFileA(".zip");
			SetCurrentDirectory("..\\");
	
			Convert.ConvertZip(CurrentFile);

		}
		else if (!strcmp(argv[1],"d"))
		{
			MZip Convert;
			char CurrentFile[MAX_PATH] = {0};
			char CurrentDirectory[MAX_PATH] = {0};
			
			GetModuleFileNameA(0,CurrentFile,sizeof(CurrentFile));
			
			StripModuleFromPath(&CurrentFile[strlen(CurrentFile)]);
			
			strcat(CurrentFile,argv[2]);

			strcpy(CurrentDirectory,CurrentFile);

			if(!Convert.RecoveryZip(CurrentFile))
			{
				return 0;
			}
		
			StripExtention(&CurrentDirectory[strlen(CurrentDirectory)]);
			printf("Unpacking %s\n",argv[2]);
			CUnzipper::Unzip(CurrentFile,CurrentDirectory,false);

			Convert.ConvertZip(argv[2]);
		}
		else
		{
			DisplayUsage();
		}
	}

	return 0;
}

void ReplaceExtension(char* FilePath,char* Extention)
{
	StripExtention(&FilePath[strlen(FilePath)]);
	strcat(FilePath,Extention);
}
void StripExtention(char* FilePath)
{
	while(*(char*)(--FilePath) != '.')
		*FilePath = 0;
	*FilePath = 0;
}


void StripModuleFromPath(char* FilePath)
{
	while(*(char*)(--FilePath) != '\\')
		*FilePath = 0;
}
